CREATE DATABASE "test_dev.project";
CREATE USER "test_dev.project" WITH password 'mmwdO1mGQV0Nqlx';
GRANT ALL privileges ON DATABASE "test_dev.project" TO "test_dev.project";

-- Для баз так же надо добавить Postgis расширения
\connect "test_dev.project";
CREATE EXTENSION IF NOT EXISTS postgis; CREATE EXTENSION IF NOT EXISTS postgis_topology; CREATE EXTENSION IF NOT EXISTS pgcrypto;

GRANT postgres TO project;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO project;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO project;
